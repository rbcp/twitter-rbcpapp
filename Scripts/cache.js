
/*
 * Cache
 */

function cache_load_and_parse_result (path) {
    try {
        var content = Ds.FileReadAllText(path, "r,ccs=UTF-8");
    } catch (e) {
        return {};
    }
    //XXX ChakraCore.dll beta returns null for missing file
    if (content != -1 /* && content != null */) {
        return JSON.parse(content);
    } else {
        print("warning: parsing failed for "+path);
        return {};
    }
}

function master_cache_load (path) {
    var master_cache = cache_load_and_parse_result(path);
    if (! ('by_id' in master_cache)) {
        master_cache.by_id = {};
    }
    if (! ('by_age' in master_cache)) {
        master_cache.by_age = [];
    }
    if (! ('blacklist' in master_cache)) {
        master_cache.blacklist = [];
    }
    return master_cache;
}

function master_cache_save (path, master_cache) {
    var vals = object_values(master_cache.by_id);
    vals.sort(function (a, b) { return b.timestamp - a.timestamp; });
    var by_age = [];
    for (var i = 0, n = vals.length; i < n; ++i) {
        by_age.push(vals[i].id_str);
    }
    master_cache.by_age = by_age;
    var master_cache_serialized = JSON.stringify(master_cache, null, ' ');
    Ds.FileWrite(path, "w,ccs=UTF-8", [master_cache_serialized]);
}

/**
 * Count blacklisted tweets. It is possible for the blacklist to have
 * tweet ids for tweets not in the cache, so this counts what actually is
 * in the cache.
 */
function master_cache_count_blacklisted (master_cache) {
    if (! ('blacklist' in master_cache)) {
        return 0;
    }
    var count = 0;
    for (var i = 0, n = master_cache.blacklist.length; i < n; ++i) {
        var id = master_cache.blacklist[i];
        if (id in master_cache.by_id) {
            count++;
        }
    }
    return count;
}

function twitterapi_validate_results (ob) {
    if (! ("results" in ob)) {
        return false;
    }
    return true;
}

function cache_format_filename (d) {
    var s = "twitterapi_" + d.getFullYear() + ("0"+(d.getMonth()+1)).slice(-2) +
        ("0" + d.getDate()).slice(-2) + "_" + ("0" + d.getHours()).slice(-2) +
        ("0" + d.getMinutes()).slice(-2) + ".json";
    return s;
}

function cache_get_path_for_twitter_handle (twitter_handle) {
    return join_paths(self_path, "cache/"+twitter_handle);
}

function cache_get_media_path_for_twitter_handle (twitter_handle) {
    return join_paths(self_path, "cache/"+twitter_handle+"/media");
}

function cache_get_master_cache_path (twitter_handle) {
    return join_paths(self_path, "cache/"+twitter_handle+"/cache.json");
}

/**
 * The save path is where scripts for particular tweets can be exported to
 * for use outside of this program.
 */
function cache_get_save_path (twitter_handle) {
    return join_paths(self_path, "save");
}


function cache_ensure_directory_structure (twitter_handle) {
    var cachedir_path = cache_get_path_for_twitter_handle(twitter_handle);
    var cache_imagedir_path = cache_get_media_path_for_twitter_handle(twitter_handle);
    Ds.DirectoryCreate(cachedir_path);
    Ds.DirectoryCreate(cache_imagedir_path);
}

function cache_get_files (twitter_handle) {
    var cachedir_path = cache_get_path_for_twitter_handle(twitter_handle);
    try {
        var files = Ds.DirectorySearch(cachedir_path, "twitterapi_*.json", false);
        var datere = RegExp("^.*[\\\\/]twitterapi_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})[^\\/]*");
        var options = [];
        for (var i = 0, n = files.length; i < n; ++i) {
            var file = files[i];
            var match = datere.exec(file);
            if (! match) {
                Ds.SendMessageWarning("unable to parse filename: "+file);
                continue;
            }
            var y = parseInt(match[1]);
            var m = parseInt(match[2]) - 1; //zero based month
            var d = parseInt(match[3]);
            var H = parseInt(match[4]);
            var M = parseInt(match[5]);
            var filedate = new Date(y, m, d, H, M);
            options.push([filedate, match[0]]);
        }
        // return most recent first
        options = options.sort(function (a, b) {
            return b[0].getTime() - a[0].getTime();
        });
        return options;
    } catch (e) {
        return [];
    }
}

function twitter_datestring_to_date (dstr) {
    // example: "Tue Nov 12 18:51:35 +0000 2019"
    var datere = /^(\w\w\w) (\w\w\w \d+) ([\d:]+) ([-+]\d\d)(\d\d) (\d\d\d\d)$/;
    var m = datere.exec(dstr);
    if (m) {
        var formatted = m[2] + ", " + m[6] + " " + m[3];
        var d = new Date(formatted);
        var tweet_tz = parseInt(m[4]) + (parseInt(m[5]) / 60);
        d.setTime(d.getTime() + ((tz - tweet_tz) * 3600000)); // convert to local time
        // print("\""+dstr+"\" -> \"" + d + "\"");
        return d;
    } else {
        throw ("Failed to parse date string: "+dstr);
    }
}

function cache_make_master_cache_entry_photo (media_spec) {
    var media_url = media_spec.media_url;
    var media_extension = take_extension(media_url);
    var size = media_spec.sizes.large;
    var aspect = size.w / size.h;
    var c = {
        url: media_url,
        extension: media_extension,
        type: "photo",
        aspect: aspect
    };
    return c;
}

function cache_make_master_cache_entry_video (media_spec) {
    var video_info = media_spec.video_info;
    if ("duration_millis" in video_info) {
        var duration = video_info.duration_millis / 1000.0;
    } else {
        duration = -1;
    }
    var variants = video_info.variants;
    var best_variant = null;
    for (var i = 0, n = variants.length; i < n; ++i) {
        var v = variants[i];
        if (v.content_type == "video/mp4") {
            if (best_variant == null || v.bitrate > best_variant.bitrate) {
                best_variant = v;
            }
        }
    }
    if (! best_variant) {
        return null;
    }
    var media_url = best_variant.url;
    var media_extension = "mp4";
    var aspect = video_info.aspect_ratio[0] / video_info.aspect_ratio[1];
    var c = {
        url: media_url,
        extension: media_extension,
        type: media_spec.type, // "video" or "animated_gif"
        duration: duration,
        aspect: aspect
    };
    return c;
}

function cache_make_master_cache_entry (tweet, mediadir_path) {
    var user_name = tweet.user.name;
    var user_screen_name = tweet.user.screen_name;
    var user_id_str = tweet.user.id_str;
    var text = tweet.text;
    if ("display_text_range" in tweet) {
        var display_text_range = tweet.display_text_range;
    } else {
        display_text_range = [0, text.length];
    }
    if ("extended_entities" in tweet) {
        var entities = tweet.extended_entities;
    } else if ("entities" in tweet) {
        entities = tweet.entities;
    } else {
        entities = [];
    }
    if ("extended_tweet" in tweet) {
        text = tweet.extended_tweet.full_text;
        display_text_range = tweet.extended_tweet.display_text_range;
        if ("extended_entities" in tweet.extended_tweet) {
            entities = tweet.extended_tweet.extended_entities;
        } else if ("entities" in tweet.extended_tweet) {
            entities = tweet.extended_tweet.entities;
        }
    }

    //XXX support replies-to-self and quotes-of-self
    if ("in_reply_to_user_id_str" in tweet &&
        tweet.in_reply_to_user_id_str &&
        tweet.in_reply_to_user_id_str != user_id_str)
    {
        // in_reply_to_status_id_str
        // in_reply_to_user_id_str
        // in_reply_to_screen_name
        return null;
    }
    if (tweet.is_quote_status) {
        if (tweet.quoted_status.user.id_str == user_id_str) {
            return null;
        } else {
            return null;
        }
    }

    // store media references.  let caller actually fetch the media.
    var media_master_cache = [];
    // var media_count = 0;
    if ("media" in entities) {
        var media = entities.media;
        for (var mi = 0, mn = media.length; mi < mn; ++mi) {
            var media_id = media[mi].id_str;

            // type: photo, animated_gif, video
            if (media[mi].type == "video" || media[mi].type == "animated_gif") {
                var c = cache_make_master_cache_entry_video(media[mi]);
                if (! c) { // fallback to the preview image
                    c = cache_make_master_cache_entry_photo(media[mi]);
                }
            } else { // either a photo or it should have an image preview
                c = cache_make_master_cache_entry_photo(media[mi]);
            }

            var media_save_filename = tweet.id_str + "_" + mi + "_" + media_id +
                "." + c.extension;
            var media_save_path = join_paths(mediadir_path, media_save_filename);

            // media_count++;
            c.id = media_id;
            c.save_filename = media_save_filename;
            c.save_path = media_save_path;
            media_master_cache.push(c);
        }
    }
    // if (media_count != 1) { // we only care about tweets with one picture
    //     return null;
    // }
    return {
        id_str: tweet.id_str,
        timestamp: (twitter_datestring_to_date(tweet.created_at)).getTime(),
        text: text.substring(display_text_range[0], display_text_range[1]),
        fulltext: text,
        media: media_master_cache
    };
}

function cache_fetch_tweet_media (tweet, force) {
    // takes the master_cache_entry of tweet, not the original record from
    // the Twitter api.
    var media = tweet.media;
    for (var mi = 0, mn = media.length; mi < mn; ++mi) {
        if (force || ! file_readable(media[mi].save_path)) {
            print("Fetching "+media[mi].type+": "+media[mi].save_filename);
            var success = Ds.FileFromURL(media[mi].save_path, media[mi].url);
            if (success != 0) {
                print("warning: failed fetch media");
            }
        }
    }
}

function cache_process_results (twitter_handle, newest_cache_filename) {
    var cachedir_path = cache_get_path_for_twitter_handle(twitter_handle);
    var mediadir_path = cache_get_media_path_for_twitter_handle(twitter_handle);

    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var results_path = join_paths(cachedir_path, newest_cache_filename);
    var tweets = cache_load_and_parse_result(results_path);
    twitterapi_validate_results(tweets);

    var nnew_tweets = 0;

    log("Updating cache...");
    for (var i = 0, n = tweets.results.length; i < n; ++i) {
        var tweet = tweets.results[i];
        var tweet_id = tweet.id_str;
        var master_cache_entry = cache_make_master_cache_entry(tweet, mediadir_path);
        if (! master_cache_entry) {
            // some tweets we don't care about, such as replies and quotes
            continue;
        }
        // tweet.in_reply_to_screen_name == "OSIRISREx" // possibly a followup on an image
        //print("Processing "+tweet_id+" ("+tweet.text.substr(0,10)+"...)");

        // Check for media
        //
        cache_fetch_tweet_media(master_cache_entry, false);

        if (! (tweet_id in master_cache.by_id)) {
            nnew_tweets++;
        }

        master_cache.by_id[tweet_id] = master_cache_entry;
    }

    master_cache_save(master_cache_file_path, master_cache);
    var ntweets = master_cache.by_age.length;
    if (ntweets > 0) {
        var tweet_id = master_cache.by_age[0];
        var tweet = master_cache.by_id[tweet_id];
        var tweet_timestamp = tweet.timestamp;
        var now = new Date().getTime();
        var age_ms = now - tweet_timestamp;
        var age = format_duration(age_ms / 1000);
        var nblacklisted = master_cache_count_blacklisted(master_cache);
        log(""+nnew_tweets+" new tweets; "+ntweets+" total in cache; "+nblacklisted+
            " blacklisted; newest from " + age + " ago.");
    } else {
        log("No new tweets; 0 total in cache.");
    }
}

function fetch_tweets (twitter_handle) {
    var now = new Date();
    var output_filename = cache_format_filename(now);

    function handler (proc) {
        // Because we're blocking while slurping the process's output, it
        // is impossible for more than one fetch to run at a time.
        log("Fetching tweets...");
        if (proc.avail()) {
            for (;proc.isRunning();) {
                if (proc.avail()) {
                    var data = proc.read();
                    print("  twitter-fetch.sh: " + data);
                }
            }
        }
        //XXX: not sure this guarantees that the process has finished.
        if (! proc.isRunning()) {
            log("Done fetching tweets");
            unregister_process_callback(proc);
            cache_process_results(twitter_handle, output_filename);
        }
    }

    var fetcher_executable = join_paths(self_filepath, "twitter-fetch.ps1");
    //Ds.SystemCommand("bash "+fetcher_executable+" "+twitter_handle);
    var proc = new Process("powershell", "-File \""+fetcher_executable+"\" "+twitter_handle+" "+output_filename);
    register_process_callback(proc, handler);
    proc.start(false /* don't open window */);
}
