
function find_image_mask (aspect) {
    var image_masks = [
        [1.000, "rounded-rectangle-aspect1000.png"],
        [1.333, "rounded-rectangle-aspect1333.png"],
        [1.718, "rounded-rectangle-aspect1718.png"]];
    var d = Number.MAX_VALUE;
    var best = 0;
    for (var i = 0, n = image_masks.length; i < n; ++i) {
        var mask = image_masks[i];
        var trying = Math.abs(aspect - mask[0]);
        if (trying < d) {
            d = trying;
            best = i;
        }
    }
    return join_paths(self_path, "images/"+image_masks[best][1]);
}

/**
 * format_script takes an array of lines and formats them as a Digistar
 * script suitable for passing to Ds.SendScriptCommands.  It prepends a
 * tab to every line (or puts it in between the timestamp and the
 * command), and appends a newline.
 */
function format_script (lines) {
    var script = "";
    var timestamp_re = RegExp('^(\\+?[\\d\\.]*)\\s*(.*)');
    for (var i = 0, n = lines.length; i < n; ++i) {
        var line = lines[i];
        var m = timestamp_re.exec(line);
        var ts = m[1];
        var command = m[2];
        script += ts + (command ? "\t" : "") + command + "\n";
    }
    return script;
}


/*
 * Predicates for filtering and finding templates
 */

function tweet_text_only_p (tweet) {
    return (tweet.media.length == 0);
}

function tweet_images_only_p (tweet) {
    for (var i = 0, n = tweet.media.length; i < n; ++i) {
        if (tweet.media[i].type != "photo") {
            return false;
        }
    }
    return (tweet.media.length > 0);
}

function tweet_videos_only_p (tweet) {
    for (var i = 0, n = tweet.media.length; i < n; ++i) {
        if (tweet.media[i].type != "video" &&
            tweet.media[i].type != "animated_gif")
        {
            return false;
        }
    }
    return (tweet.media.length > 0);
}



var default_templates = [
    {
        name: "oneup-video",
        predicate: function (tweet) {
            return (tweet.media.length == 1 &&
                    (tweet.media[0].type == "video" ||
                     tweet.media[0].type == "animated_gif"));
        },
        on: function (twitterob, tweet, u) {
            var vid = "tweet_video"+u;
            var model = "tweet_video_model"+u;
            var dist = 15 * Math.sqrt(tweet.media[0].aspect);
            return [
                vid+" is videoClass",
                vid+" path "+tweet.media[0].save_path,
                model+" is solidModelClass",
                model+" model $Content/Library/Models/Misc/image.x",
                model+" modeltexture 0 "+vid,
                model+" modeltexture 2 "+find_image_mask(tweet.media[0].aspect),
                model+" position spherical -45 45 "+dist,
                model+" attitude 135 -45 0",
                model+" scale "+tweet.media[0].aspect+" 1 1",
                model+" status on",
                model+" intensity 0",
                "1 scene add "+model+" near",
                vid+" loop",
                model+" intensity 100 duration 2"];
        },
        off: function (twitterob, tweet, u) {
            var vid = "tweet_video"+u;
            var model = "tweet_video_model"+u;
            return [
                vid+" is videoClass",
                model+" is solidModelClass",
                model+" intensity 0 duration 2",
                vid+" volume 0 duration 2",
                "2 "+model+" delete",
                vid+" delete"];
        }
    },
    {
        name: "oneup",
        predicate: function (tweet) { return (tweet.media.length == 1); },
        on: function (twitterob, tweet, u) {
            var img = "tweet_image"+u;
            var dist = 15 * Math.sqrt(tweet.media[0].aspect);
            return [
                img+" is solidModelClass",
                img+" model $Content/Library/Models/Misc/image.x",
                img+" modeltexture 0 "+tweet.media[0].save_path,
                img+" modeltexture 2 "+find_image_mask(tweet.media[0].aspect),
                img+" position spherical -45 45 "+dist,
                img+" attitude 135 -45 0",
                img+" scale "+tweet.media[0].aspect+" 1 1",
                img+" status on",
                img+" intensity 0",
                "1 scene add "+img+" near",
                img+" intensity 100 duration 2"];
        },
        off: function (twitterob, tweet, u) {
            var img = "tweet_image"+u;
            return [
                img+" is solidModelClass",
                img+" intensity 0 duration 2",
                "2 "+img+" delete"];
        }
    },
    {
        name: "justtext",
        predicate: function (tweet) { return true; },
        on: function (twitterob, tweet, u) {
        },
        off: function (twitterob, tweet, u) {
        }
    }
];

var templates = {};

function default_text_template (tweet, tweet_blacklisted_p) {
    var pages = text_fill_for_ds_objects(tweet.text, 60, 3);
    var script = "";
    for (var i = 0; i < 2; ++i) {
        var page = pages[i] || " ";
        script += "\ttwitter_rbcpapp tweetText"+(i+1)+" \"" +
            page + "\"\n";
    }
    Ds.SendScriptCommands("twitter-rbcpapp-notes", script);
}

var text_templates = {};

function templates_register_internal (t, twitter_handle, template_script_path) {
    templates[twitter_handle] = t;
}

function text_template_register_internal (t, twitter_handle, template_script_path) {
    text_templates[twitter_handle] = t;
}


var template_uniquifier = 0;

function template_find (twitter_handle, tweet) {
    var templates_for_handle = templates[twitter_handle]
    if (templates_for_handle) {
        for (var i = 0, n = templates_for_handle.length; i < n; ++i) {
            var template_rec = templates_for_handle[i];
            if (template_rec.predicate(tweet)) {
                return {
                    __proto__: template_rec,
                    tweet: tweet,
                    uniquifier: (template_uniquifier++)
                };
            }
        }
    }
    // Check in default templates
    for (var i = 0, n = default_templates.length; i < n; ++i) {
        var template_rec = default_templates[i];
        if (template_rec.predicate(tweet)) {
            return {
                __proto__: template_rec,
                tweet: tweet,
                uniquifier: (template_uniquifier++)
            };
        }
    }
    return null;
}

function text_template_find (twitter_handle) {
    if (twitter_handle in text_templates) {
        return text_templates[twitter_handle];
    }
    return default_text_template;
}
