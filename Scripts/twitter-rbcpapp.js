
var twitter_rbcpapp_version = "2.1.1";


/*
 * Globals: Configuration
 */

var expiration_ms = 1000 * 60 * 10; // 10 minutes


/*
 * Globals: Internal
 */

var tz;
var self_path;
var self_filepath;
var current_twitter_object;
var current_showing_tweet = null;
var current_template_rec;
var twitter_rbcpapp_global_manager = "twitter_rbcpapp";

//XXX tweet_hide_hook is a bit weird - it would be more consistent with
//    tweet templates to have a text_template be an object with both an
//    'on' and an 'off', or maybe 'on' and 'hide'.
var tweet_hide_hook = [];
var global_show_blacklisted = false;

include("./utils.js");


/*
 * Callback registration
 */

var ds_object_attribute_handlers = {};
var ds_object_command_handlers = {};
var ds_process_handlers = [];

function register_attribute_callback (dsob, attr, handler) {
    var k = dsob+"."+attr;
    var old_handler = ds_object_attribute_handlers[k];
    if (old_handler == handler) { // already registered
        return;
    }
    ds_object_attribute_handlers[k] = handler;
    if (old_handler) {
        Ds.SendMessageWarning("register_attribute_callback changing handler of "+dsob+" "+attr);
    } else {
        Ds.AddObjectAttrEvent(dsob, attr);
    }
}

function register_command_callback (dsob, command, handler) {
    var k = dsob+"."+command;
    var old_handler = ds_object_command_handlers[k];
    if (old_handler == handler) { // already registered
        return;
    }
    ds_object_command_handlers[k] = handler;
    if (old_handler) {
        Ds.SendMessageWarning("register_command_callback changing handler of "+dsob+" "+command);
    } else {
        Ds.AddObjectCommandEvent(dsob, command);
    }
}

function register_process_callback (proc, handler) {
    var notification_ref = new NotificationRef(proc);
    ds_process_handlers.push([notification_ref, proc, handler]);
    Ds.AddNotificationEvent(notification_ref);
}

function unregister_process_callback (proc) {
    var handler_rec = null;
    var i;
    for (i = 0, n = ds_process_handlers.length; i < n; ++i) {
        handler_rec = ds_process_handlers[i];
        if (handler_rec[1] == proc) {
            break;
        }
        handler_rec = null;
    }
    if (handler_rec) {
        ds_process_handlers.splice(i, 1);
        Ds.RemoveNotificationEvent(handler_rec[0]);
    }
}


include("./cache.js");

include("./template.js");


function log_tweet_showing_message (tweet) {
    var logmsg = "Showing tweet from " +
        timestamp_to_human_readable_date(tweet.timestamp);
    if (tweet.media.length > 0) {
        logmsg += " ("+tweet.media[0].type+")";
    } else {
        logmsg += " (text only)";
    }
    log(logmsg);
}

function write_tweet_text (twitter_handle, tweet, tweet_blacklisted_p) {
    var text_template = text_template_find(twitter_handle);
    if (text_template) {
        text_template(tweet, tweet_blacklisted_p);
    }
}

var filters = {};

function default_filter (twitter_handle, tweet) {
    return true;
}

function filter_register_internal (f, twitter_handle, template_script_path) {
    filters[twitter_handle] = f;
}

function filter_tweet (dsob, tweet) {
    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (tweet_text_only_p(tweet)) {
        if (! Ds.GetObjectAttr(twitter_rbcpapp_global_manager, "showTextTweets") ||
            ! Ds.GetObjectAttr(dsob, "showTextTweets"))
        {
            return false;
        }
    }
    if (tweet_images_only_p(tweet)) {
        if (! Ds.GetObjectAttr(twitter_rbcpapp_global_manager, "showImageTweets") ||
            ! Ds.GetObjectAttr(dsob, "showImageTweets"))
        {
            return false;
        }
    }
    if (tweet_videos_only_p(tweet)) {
        if (! Ds.GetObjectAttr(twitter_rbcpapp_global_manager, "showVideoTweets") ||
            ! Ds.GetObjectAttr(dsob, "showVideoTweets"))
        {
            return false;
        }
    }
    // check filter installed by customizeScript
    if (twitter_handle in filters) {
        var f = filters[twitter_handle](twitter_handle, tweet);
        if (f === false) {
            return false;
        }
    }
    return default_filter(twitter_handle, tweet);
}

function customize_script_load (dsob) {
    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    var customize_script = Ds.GetObjectAttr(dsob, "customizeScript");

    /*
     * API for Customize Scripts
     */
    function filter_register (f) {
        filter_register_internal(f, twitter_handle, customize_script);
    }
    function templates_register (t) {
        templates_register_internal(t, twitter_handle, customize_script);
    }
    function text_template_register (t) {
        text_template_register_internal(t, twitter_handle, customize_script);
    }

    if (file_readable(customize_script)) {
        // customize_script will register a set of templates with the current handle
        // can we provide the register method as a closure?
        include(customize_script);
    }
}


/*
 * Callbacks: Cache / Management
 */

function twitterfeedclass_blacklist_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    if (! current_showing_tweet) {
        log("No tweet loaded.");
        return;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling blacklist.");
        return;
    }

    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var tweet = master_cache.by_id[current_showing_tweet];

    if (master_cache.blacklist.indexOf(current_showing_tweet) == -1) {
        master_cache.blacklist.unshift(current_showing_tweet);
        master_cache_save(master_cache_file_path, master_cache);
        write_tweet_text(twitter_handle, tweet, true);
    }
    log("Will not show this tweet again. ("+current_showing_tweet+")");
}

function twitterfeedclass_unblacklist_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    if (! current_showing_tweet) {
        log("No tweet loaded.");
        return;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling unblacklist.");
        return;
    }

    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var tweet = master_cache.by_id[current_showing_tweet];

    var blacklist_index = master_cache.blacklist.indexOf(current_showing_tweet);
    if (blacklist_index == -1) {
        log("This tweet is not blacklisted.");
    } else {
        master_cache.blacklist.splice(blacklist_index, 1);
        master_cache_save(master_cache_file_path, master_cache);
        write_tweet_text(twitter_handle, tweet, false);
        log("This tweet has been unblacklisted. ("+current_showing_tweet+")");
    }
}

function twitterfeedclass_cacheinfo_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling cacheinfo.");
        return;
    }

    // Get newest tweet from master cache
    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var by_age = master_cache.by_age;
    var ntweets = by_age.length;
    if (ntweets < 1) {
        var msg = "No tweets in cache; please fetch.";
    } else {
        var tweet_id = master_cache.by_age[0];
        var tweet = master_cache.by_id[tweet_id];
        var tweet_timestamp = tweet.timestamp;
        var now = new Date().getTime();
        var age_ms = now - tweet_timestamp;
        var age = format_duration_short(age_ms / 1000);
        var nblacklisted = master_cache_count_blacklisted(master_cache);
        msg = "" + ntweets + " tweets cached; "+
            nblacklisted+" blacklisted; newest from " + age + " ago.";
    }
    log(msg);
}

function twitterfeedclass_fetch_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling fetch.");
        return;
    }

    cache_ensure_directory_structure(twitter_handle);

    //XXX this should be checking the master cache, not the raw twitter result cache
    var cache_files = cache_get_files(twitter_handle);
    if (cache_files.length < 1) {
        log("Nothing in cache.  Will fetch tweets.");
        fetch_tweets(twitter_handle);
        return;
    }
    var latest_cache = cache_files[0];
    var latest_cache_ts = latest_cache[0].getTime();
    var now = new Date();
    var now_ts = now.getTime();

    print("Latest cache time: "+latest_cache[0]+" ("+(latest_cache[0].getTime())+")");
    print("Now: "+now+" ("+(now.getTime())+")");
    print("Cache is "+format_duration_short((now_ts - latest_cache_ts) / 1000)+" old");
    if (now_ts - latest_cache_ts > expiration_ms) {
        log("Cache is expired.  Will fetch tweets.");
        fetch_tweets(twitter_handle);
        return;
    }

    //XXX print how many tweets ready to go
    log("Cache is up to date.");
}

function fetchmedia_callback () {
    if (! current_showing_tweet) {
        log("No tweet loaded.");
        return;
    }
    var dsob = current_twitter_object;
    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle.");
        return;
    }
    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);
    var tweet = master_cache.by_id[current_showing_tweet];
    log("Fetching media...");
    // Hide it
    if (current_template_rec) {
        var template_off_fn = current_template_rec.off;
        var script = template_off_fn(dsob, current_template_rec.tweet,
                                     current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-off"
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
        run_hooks(tweet_hide_hook, dsob);
    }
    cache_fetch_tweet_media(tweet, true);
    log("Fetching media...done");
    // get a new template to update uniquifier
    var tweet_blacklisted_p = (master_cache.blacklist.indexOf(current_showing_tweet) > -1);
    current_template_rec = template_find(twitter_handle, tweet);
    if (current_template_rec) {
        var template_fn = current_template_rec.on;
        var script = template_fn(dsob, tweet, current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-on";
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
    }

    log_tweet_showing_message(tweet);
    write_tweet_text(twitter_handle, tweet, tweet_blacklisted_p);
}

/*
 * Callbacks: Display
 */

function twitterfeedclass_hide_callback (dsob, cmd) {
    //XXX you could call hide after switching to a different feed, so dsob may be wrong here.

    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    // turn off previous
    if (current_template_rec) {
        var template_off_fn = current_template_rec.off;
        //XXX: when quitting, dsob will be undefined
        var script = template_off_fn(dsob, current_template_rec.tweet,
                                     current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-off"
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
        run_hooks(tweet_hide_hook, dsob);
        log("Tweet display off");
    }
}

function twitterfeedclass_next_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    var show_blacklisted = global_show_blacklisted ||
        Ds.GetObjectAttr(twitter_rbcpapp_global_manager, "showBlacklisted") ||
        Ds.GetObjectAttr(dsob, "showBlacklisted");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling next.");
        return;
    }

    // Get newest tweet from master cache
    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var by_id = master_cache.by_id;
    var by_age = master_cache.by_age;
    var ntweets = by_age.length;
    if (ntweets < 1) {
        print("No tweets in cache; please fetch.");
        return;
    }

    // if current_showing_tweet is null or an ID from another feed, this
    // index will be -1, and we'll start from the beginning of the current
    // feed.
    var i = by_age.indexOf(current_showing_tweet);
    for (i = i + 1; i < ntweets; ++i) {
        var tweet_blacklisted_p = (master_cache.blacklist.indexOf(by_age[i]) > -1);
        var filter_pass = filter_tweet(dsob, by_id[by_age[i]]);
        if (filter_pass && (show_blacklisted || ! tweet_blacklisted_p)) {
            var tweet_id = by_age[i];
            var tweet = by_id[tweet_id];
            current_showing_tweet = tweet_id;
            break;
        }
    }
    if (! tweet_id) {
        if (show_blacklisted) {
            log("No tweets to show.");
        } else {
            log("No non-blacklisted tweets to show.");
        }
        return;
    }

    // turn off previous
    if (current_template_rec) {
        var template_off_fn = current_template_rec.off;
        var script = template_off_fn(dsob, current_template_rec.tweet,
                                     current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-off";
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
    }

    // get a template
    current_template_rec = template_find(twitter_handle, tweet);
    if (current_template_rec) {
        var template_fn = current_template_rec.on;
        var script = template_fn(dsob, tweet, current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-on";
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
    }

    log_tweet_showing_message(tweet);
    write_tweet_text(twitter_handle, tweet, tweet_blacklisted_p);
}

function twitterfeedclass_previous_callback (dsob, cmd) {
    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    var show_blacklisted = global_show_blacklisted ||
        Ds.GetObjectAttr(twitter_rbcpapp_global_manager, "showBlacklisted") ||
        Ds.GetObjectAttr(dsob, "showBlacklisted");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling previous.");
        return;
    }

    // Get newest tweet from master cache
    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);

    var by_id = master_cache.by_id;
    var by_age = master_cache.by_age;
    var ntweets = by_age.length;
    if (ntweets < 1) {
        print("No tweets in cache; please fetch.");
        return;
    }

    // if current_showing_tweet is null or an ID from another feed, we
    // will start counting from -1, which will not find any previous
    // tweet, and display an appropriate error message.
    var i = by_age.indexOf(current_showing_tweet);
    for (i = i - 1; i >= 0; --i) {
        var tweet_blacklisted_p = (master_cache.blacklist.indexOf(by_age[i]) > -1);
        var filter_pass = filter_tweet(dsob, by_id[by_age[i]]);
        if (filter_pass && (show_blacklisted || ! tweet_blacklisted_p)) {
            var tweet_id = by_age[i];
            var tweet = master_cache.by_id[tweet_id];
            current_showing_tweet = tweet_id;
            break;
        }
    }
    if (! tweet_id) {
        if (show_blacklisted) {
            log("No tweets to show.");
        } else {
            log("No non-blacklisted tweets to show.");
        }
        return;
    }

    // turn off previous
    if (current_template_rec) {
        var template_off_fn = current_template_rec.off;
        var script = template_off_fn(dsob, current_template_rec.tweet,
                                     current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-off";
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
    }

    // get a template
    current_template_rec = template_find(twitter_handle, tweet);
    if (current_template_rec) {
        var template_fn = current_template_rec.on;
        var script = template_fn(dsob, tweet, current_template_rec.uniquifier);
        var scriptname = "twitter-rbcpapp-"+current_template_rec.name+"-on";
        if (script) {
            Ds.SendScriptCommands(scriptname, format_script(script));
        }
    }

    log_tweet_showing_message(tweet);
    write_tweet_text(twitter_handle, tweet, tweet_blacklisted_p);
}


function twitterfeedclass_save_callback (dsob, cmd) {
    function media_for_save (media_list) {
        var res = [];
        for (var i = 0, n = media_list.length; i < n; ++i) {
            var m = object_clone(media_list[i]);
            m.save_path = "./" + m.save_filename;
            res.push(m);
        }
        return res;
    }

    if (! dsob || dsob == twitter_rbcpapp_global_manager) {
        dsob = current_twitter_object;
    }

    var twitter_handle = Ds.GetObjectAttr(dsob, "twitterHandle");
    if (twitter_handle == "") {
        print("Please set "+dsob+" twitterHandle before calling save.");
        return;
    }

    var master_cache_file_path = cache_get_master_cache_path(twitter_handle);
    var master_cache = master_cache_load(master_cache_file_path);
    var by_id = master_cache.by_id;
    var tweet_id = current_showing_tweet;
    var tweet = by_id[current_showing_tweet];
    if (! tweet) {
        log("Nothing to do. Load a tweet.");
        return;
    }

    // get a template
    var template_rec = template_find(twitter_handle, tweet);
    if (template_rec) {
        var tweet_for_save = {
            __proto__: tweet,
            media: media_for_save(tweet.media)
        };
        template_rec.uniquifier = "_save" + template_rec.uniquifier;
        var template_on_fn = current_template_rec.on;
        var template_off_fn = current_template_rec.off;
        var on_script_content = template_on_fn(dsob, tweet_for_save, template_rec.uniquifier);
        var off_script_content = template_off_fn(dsob, tweet_for_save, template_rec.uniquifier);
        var script_basename = twitter_handle.toLowerCase() + "-" +
            timestamp_to_iso8601short(tweet.timestamp);
        var savedir_path = cache_get_save_path(twitter_handle);
        var tweet_savedir_path = join_paths(savedir_path, script_basename);
        var on_script_filename = script_basename + "-on.ds";
        var off_script_filename = script_basename + "-off.ds";
        var on_script_path = join_paths(tweet_savedir_path, on_script_filename);
        var off_script_path = join_paths(tweet_savedir_path, off_script_filename);

        Ds.DirectoryCreate(tweet_savedir_path);
        if (on_script_content) {
            Ds.FileWrite(on_script_path, "w,ccs=UTF-8",
                         [format_script(on_script_content)]);
        }
        if (off_script_content) {
            Ds.FileWrite(off_script_path, "w,ccs=UTF-8",
                         [format_script(off_script_content)]);
        }
        var media = tweet_for_save.media;
        for (var i = 0, n = media.length; i < n; ++i) {
            var media_save_path = join_paths(tweet_savedir_path, media[i].save_filename);
            var success = Ds.FileFromURL(media_save_path, media[i].url);
            if (success != 0) {
                print("warning: failed fetch media");
            }
        }
        log("Saved this tweet to "+tweet_savedir_path);
        file_explorer(tweet_savedir_path);
    }
}


/*
 * Main
 */

function register_all_callbacks (dsob) {
    register_command_callback(dsob, "blacklist", twitterfeedclass_blacklist_callback);
    register_command_callback(dsob, "unblacklist", twitterfeedclass_unblacklist_callback);
    register_command_callback(dsob, "cacheinfo", twitterfeedclass_cacheinfo_callback);
    register_command_callback(dsob, "fetch", twitterfeedclass_fetch_callback);
    register_command_callback(dsob, "hide", twitterfeedclass_hide_callback);
    register_command_callback(dsob, "next", twitterfeedclass_next_callback);
    register_command_callback(dsob, "previous", twitterfeedclass_previous_callback);
    register_command_callback(dsob, "save", twitterfeedclass_save_callback);
}

function twitterfeedclass_object_add (dsob) {
    current_twitter_object = dsob;

    customize_script_load(dsob);

    register_all_callbacks(dsob);
}

function start () {
    self_path = get_self_path();
    self_filepath = Ds.ResolvePathName(self_path);
    //XXX: ChakraCore appears to handle DST, so we are likely screwing up
    //     dates by grabbing our tz from Digistar and applying it to all
    //     date calculations.
    tz = Ds.GetObjectAttr("host", "timezone");
    var running = true;
    var exit_message = "Twitter RBCPApp stopped";
    log("Twitter RBCPApp "+twitter_rbcpapp_version+" started");

    Ds.CreateObject(twitter_rbcpapp_global_manager, "twitterClass");
    register_all_callbacks(twitter_rbcpapp_global_manager);

    if (! file_readable(join_paths(self_path, "credentials.txt"))) {
        exit_message = "Credentials not found. Please read the manual for how to setup.";
        running = false;
    }

    // Ds.SetTimerEvent(1e12, 'system'); //XXX: hack for D5 16.04
    Ds.SetMessageEvent();

    var message_handlers = {
        add: function (arg) {
            twitterfeedclass_object_add(arg);
        },
        showblacklisted: function (arg) {
            global_show_blacklisted = (arg == "on");
        },
        blacklist: twitterfeedclass_blacklist_callback,
        unblacklist: twitterfeedclass_unblacklist_callback,
        cacheinfo: twitterfeedclass_cacheinfo_callback,
        fetch: twitterfeedclass_fetch_callback,
        fetchmedia: fetchmedia_callback,
        hide: twitterfeedclass_hide_callback,
        next: twitterfeedclass_next_callback,
        previous: twitterfeedclass_previous_callback,
        save: twitterfeedclass_save_callback,
        quit: function () {
            twitterfeedclass_hide_callback();
            running = false;
        },
        fadestopreset: function () {
            // no need to hide tweets on a fadestopreset
            running = false;
        }
    };

    function process (event) {
        switch (typeof event) {
        case "DsObjectAttrRef":
            var dsob = Ds.GetAttrRefObjectName(event);
            var attr = Ds.GetAttrRefAttrName(event);
            var handler = ds_object_attribute_handlers[dsob+"."+attr];
            handler(dsob, attr);
            break;
        case "DsObjectCommandRef":
            var dsob = Ds.GetCommandRefObjectName(event);
            var command = Ds.GetCommandRefCommandName(event);
            var handler = ds_object_command_handlers[dsob+"."+command];
            handler(dsob, command);
            break;
        case "number": // timer interval
            // sleep_callback(event);
            break;
        case "string": // control message
            var esp = event.split(/\s+/);
            var ecmd = esp[0];
            var handler = message_handlers[ecmd];
            if (handler) {
                handler(esp[1]);
            } else {
                log("ignored unknown command: "+event);
            }
            break;
        case "NotificationRef": // processes, etc
            var handlerinfo = ds_process_handlers.find(function (x) { return (x[0] == event); });
            if (handlerinfo) {
                var proc = handlerinfo[1];
                var handler = handlerinfo[2];
                handler(proc);
            } else {
                print("Warning: notification from unknown process");
            }
            break;
        }
    }

    while (running) {
        var event = Ds.WaitForEvent();
        if (typeof event == "object") {
            for (var i = 0, n = event.length; i < n; ++i) {
                process(event[i]);
            }
        } else {
            process(event);
        }
    }
    //XXX error - not sure why
    // Ds.DeleteObject(twitter_rbcpapp_global_manager);
    log(exit_message);
}
