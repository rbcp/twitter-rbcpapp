
/*
 * Utils
 */

function log (msg) {
    Ds.SetObjectAttr("system", "message", msg);
}

function pp (o) {
    for (var k in o) {
        print(k+": "+o[k]);
    }
}

function file_readable (pathname) {
    return (Ds.FileAccess(pathname, 4) == 0);
}

function join_paths (a, b) {
    var alast = a[a.length-1];
    var bfirst = b[0] || "";
    if (bfirst == ".") {
        var bsp = b.split(/[\/\\]/);
        var adrop = 0;
        var bskip = 0;
        for (var i = 0, n = bsp.length; i < n; ++i) {
            if (bsp[i] == ".") {
                bskip++;
            } else if (bsp[i] == "..") {
                bskip++;
                adrop++;
            } else {
                break;
            }
        }
        if (adrop > 0) {
            if (alast == "\\" || alast == "/") {
                adrop++;
            }
            var asp = a.split(/[\\\/]/);
            var aspn = asp.length;
            asp.splice(aspn - adrop, adrop);
            a = asp.join("/");
        }
        bsp.splice(0, bskip);
        b = bsp.join("/");
        bfirst = b[0] || "";
    }
    alast = a[a.length-1];
    if (alast == "\\" || alast == "/" || bfirst == "\\" || bfirst == "/") {
        var sep = "";
    } else {
        sep = "/";
    }
    return a + sep + b;
}

function take_extension (pathname) {
    var dot = pathname.lastIndexOf('.');
    var ext = pathname.substr(dot + 1);
    return ext;
}

function drop_filename (pathname) {
    var slash = pathname.lastIndexOf('/');
    var backslash = pathname.lastIndexOf('\\');
    return pathname.substr(0, Math.max(slash, backslash)+1);
}

function get_self_path () {
    var self = Ds.GetObjectAttr("js", "play");
    var scripts_dir = drop_filename(self.pathName);
    return join_paths(scripts_dir, "..");
}


/*
 * Utils: String
 */

function format_duration (s) {
    var days = Math.floor(s / 86400);
    s -= days * 86400;
    var hours = Math.floor(s / 3600);
    s -= hours * 3600;
    var minutes = Math.floor(s / 60);
    s -= minutes * 60;
    var str = "" +
        (days > 0 ? (days + " days, ") : "") +
        (hours > 0 ? (hours + " hours, ") : "") +
        minutes + " minutes";
    return str;
}

function format_duration_short (s) {
    var days = Math.floor(s / 86400);
    s -= days * 86400;
    var hours = Math.floor(s / 3600);
    s -= hours * 3600;
    var minutes = Math.floor(s / 60);
    s -= minutes * 60;
    var str = "" +
        (days > 0 ? (days + "d ") : "") +
        (hours > 0 ? (hours + "h ") : "") +
        minutes + "m";
    return str;
}

function timestamp_to_human_readable_date (ts) {
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday"];
    var d = new Date(ts);
    var m = d.getMonth()+1;
    m = (m < 10 ? "0" : "") + m;
    var dd = d.getDate();
    dd = (dd < 10 ? "0" : "") + dd;
    return d.getFullYear()+"-"+m+"-"+dd+
        " ("+days[d.getDay()]+")";
}

function timestamp_to_iso8601short (ts) {
    var d = new Date(ts);
    return ""+d.getFullYear()+(("0"+d.getMonth()+1).slice(-2))+
        (("0"+d.getDate()).slice(-2))+
        "T"+(("0"+d.getHours()).slice(-2))+
        (("0"+d.getMinutes()).slice(-2));
}


function text_fill_for_ds_objects (text, fill_column, lines_per_page) {
    var words = text.split(/\s/);
    var lines = [];
    var line = "";
    var line_len = 0;
    for (var i = 0, n = words.length; i < n; ++i) {
        var word = words[i];
        var word_len = word.length;
        if (line_len + word_len < fill_column) {
            //XXX D6 has an odd design choice whereby a space followed by
            //    a comment character breaks string parsing. An underscore
            //    in this position will be displayed as a space. We use
            //    another workaround: a non-breaking space character.
            if (line_len > 0) {
                line += ((word[0] == '#' || word[0] == ';') ? "\u00a0" : " ");
                line_len += 1;
            }
            line += word;
            line_len += word_len;
        } else {
            lines.push(line);
            print(line);
            line = word;
            line_len = word_len;
        }
    }
    lines.push(line);

    var pages = [];
    for (var i = 0, n = lines.length; i < n; i += lines_per_page) {
        var pagelines = lines.slice(i, i + lines_per_page);
        for (var j = 0; j < lines_per_page; ++j) {
            if (pagelines[j] === undefined) { // TinyJS workaround
                pagelines[j] = " ";
            }
        }
        var page = pagelines.join("|");
        pages.push(page);
    }
    return pages;
}


/*
 * Utils: Object
 */

function object_clone (o) {
    var clone = {};
    for (var k in o) {
        clone[k] = o[k];
    }
    return clone;
}

function object_values (o) {
    var vals = [];
    for (var k in o) {
        vals.push(o[k]);
    }
    return vals;
}


/*
 * Hooks
 */

function run_hooks (hook) {
    var args = [];
    for (var i = 1, n = arguments.length; i < n; ++i) {
        args.push(arguments[i]);
    }
    for (var i = 0, n = hook.length; i < n; ++i) {
        var f = hook[i];
        f.apply(null, args);
    }
}


/*
 * Windows
 */

function file_explorer (path) {
    var resolved = Ds.ResolvePathName(path);
    Ds.SystemCommand("explorer "+resolved);
}
