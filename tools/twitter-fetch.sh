#!/bin/sh

function usage {
    echo "usage: twitter-fetch.sh <twitter_handle> <output_filename>"
}

self="$0"
self_path=$(dirname "$self")

twitter_handle="$1"

output_filename="$2"

if [[ -z "$twitter_handle" ]]; then
    exit 1
fi

if [[ -z "$output_filename" ]]; then
    usage
    exit 1
fi

credentials_path="$self_path\\credentials.txt"

#datefrom=$(date -u +%Y%m%d%H%M --date="30 days ago")
#datenow=$(date -u +%Y%m%d_%H%M)

cache_path="$self_path\\cache\\$twitter_handle"

## twitter-rbcpapp.js also ensures this directory exists, but we'll leave
## this here for command line use.
mkdir -p "$cache_path"

#outfile="$cache_path\\$datenow.json"
outfile="$cache_path\\$output_filename"

. "$credentials_path"

curl --request POST \
  --url https://api.twitter.com/1.1/tweets/search/30day/$devenvname.json \
  --header "authorization: Bearer $bearer_token" \
  --header 'content-type: application/json' \
  --data '{"query":"from:'"$twitter_handle"' lang:en"}' > "$outfile"
