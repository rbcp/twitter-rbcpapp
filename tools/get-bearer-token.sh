#!/bin/sh

. ../credentials.txt

curl -u "$apikey:$apisecretkey" \
  --data 'grant_type=client_credentials' \
  'https://api.twitter.com/oauth2/token'
