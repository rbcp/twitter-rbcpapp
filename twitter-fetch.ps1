
param ([Parameter(Mandatory)]$twitter_handle, `
       [Parameter(Mandatory)]$output_filename)

$self_dir = Split-Path -Parent $MyInvocation.MyCommand.Path
$credentials_path = Join-Path $self_dir "credentials.txt"
$cache_path = Join-Path $self_dir cache
$cache_path = Join-Path $cache_path $twitter_handle
$outfile = Join-Path $cache_path $output_filename

## Read credentials file
##
$credentials = @{}
If (! (Test-Path -path $credentials_path)) {
    throw "credentials.txt not found"
}
Get-Content $credentials_path | Foreach-Object{
    $var = $_.Split('=')
    If ($var[0] -ne '') {
        $val = $var[1].Trim('"')
        $credentials[ $var[0] ] = $val
    }
}
If ($credentials['apikey'] -eq "") {
    throw "apikey missing in credentials.txt"
}
If ($credentials['apisecretkey'] -eq "") {
    throw "apisecretkey missing in credentials.txt"
}
If ($credentials['bearer_token'] -eq "") {
    throw "bearer_token missing in credentials.txt"
}
If ($credentials['appname'] -eq "") {
    throw "appname missing in credentials.txt"
}
If ($credentials['devenvname'] -eq "") {
    throw "devenvname missing in credentials.txt"
}


## Ensure cache directory exists
##
If (! (Test-Path -path $cache_path)) {
    New-Item -ItemType directory -Path $cache_path
}



[Net.ServicePointManager]::SecurityProtocol = `
  [Net.SecurityProtocolType]::Tls `
  -bor [Net.SecurityProtocolType]::Tls11 `
  -bor [Net.SecurityProtocolType]::Tls12

$headers = @{ 'Authorization' = "Bearer $($credentials['bearer_token'])" }
$body = ConvertTo-Json @{'query' = "from:$twitter_handle lang:en" }

Invoke-RestMethod `
  -Uri "https://api.twitter.com/1.1/tweets/search/30day/$($credentials['devenvname']).json" `
  -Method POST `
  -ContentType "application/json" `
  -Headers $headers `
  -Body $body `
  -OutFile $outfile

Write-Host "Done"
