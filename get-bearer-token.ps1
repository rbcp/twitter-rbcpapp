
$self_dir = Split-Path -Parent $MyInvocation.MyCommand.Path
$credentials_path = Join-Path $self_dir "credentials.txt"

## Read credentials file
##
$credentials = @{}
If (! (Test-Path -path $credentials_path)) {
    throw "credentials.txt not found"
}
Get-Content $credentials_path | Foreach-Object{
    $var = $_.Split('=')
    If ($var[0] -ne '') {
        $val = $var[1].Trim('"')
        $credentials[ $var[0] ] = $val
    }
}
If ($credentials['apikey'] -eq "") {
    throw "apikey missing in credentials.txt"
}
If ($credentials['apisecretkey'] -eq "") {
    throw "apisecretkey missing in credentials.txt"
}

## Get token
##
[Net.ServicePointManager]::SecurityProtocol = `
  [Net.SecurityProtocolType]::Tls `
  -bor [Net.SecurityProtocolType]::Tls11 `
  -bor [Net.SecurityProtocolType]::Tls12

$pair = "$($credentials['apikey']):$($credentials['apisecretkey'])"
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes($pair))

$response = Invoke-RestMethod -Uri "https://api.twitter.com/oauth2/token" `
  -Method POST `
  -ContentType "application/x-www-form-urlencoded" `
  -Headers @{ Authorization = "Basic $base64AuthInfo" } `
  -Body @{ grant_type = "client_credentials" }

$response.access_token
