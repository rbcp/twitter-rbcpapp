# Twitter RBCPApp


## Important Note

To use this program, you will need to sign up for a Twitter Developer account and submit an application to use the Twitter API.

You will also need to rename and customize some files for your installation. Please read and follow this manual to use Twitter RBCPApp.


## Features

* Basic operation from provided Control Panel page.
* Advanced operation by scripting - highly customizable.
* Display tweet text in the control panel.
* Customizable templates for displaying both tweets and their media items.
* Templates included for single photo or video. Flexable template system allows for more complex displays to be added.
* Tweets and media are stored in a cache for reliability. (Internet outages shouldn't break your show.)
* Tweets may be blacklisted and unblacklisted.
* Provides twitterFeedClass, an object for interacting with a Twitter feed.
* Templates allow for tweets to be attached to their relevant object in space, or just splatted on the dome.
* Tweets can be exported for use outside of this app.



## Setup

1. Copy `twitter-fetch.ps1.txt` to `twitter-fetch.ps1`. This step is necessary because Cloud items cannot have files in them with the extension `.ps1`, which is PowerShell Script. This script is necessary because it is what downloads tweets.
1. Copy `get-bearer-token.ps1.txt` to `get-bearer-token.ps1`. This PowerShell script will be used by a later step in these instructions.
1. Get a Twitter Developer account
1. On your account, add an App. You can make up the app name and development environment name. For example, I used "rbcp" and "rbcpenv" (RBCP are the initials of my planetarium.)
1. When your app is approved, find its "API Key" and "API Secret Key" in the "Keys and Tokens" tab.
1. In the twitter-rbcpapp directory, make a copy of `credentials.template.txt` called `credentials.txt` and edit it, filling in the APIKey, APISecretKey, AppNam, and Development Environment Name that you got from Twitter.
1. The last piece of information you need to put in credentials.txt is called the "Bearer Token", and you need to generate this. An explanation of the bearer token can be found [here](https://developer.twitter.com/en/docs/authentication/oauth-2-0/bearer-tokens). This is where you can use `get-bearer-token.ps1` to fetch your bearer token. After it runs successfully, copy the hash that it outputs into the provided location in credentials.txt.


## Testing

Twitter RBCPApp calls out to a PowerShell program called *twitter-fetch.ps1* to fetch tweets. If things are not working, test this program directly in a PowerShell window and see if it is successful or if it gives an error. An example invocation looks as follows:

```
./twitter-fetch.ps1 NASA foo.json
```

This PowerShell program reads from your credentials.txt, performs a Twitter API query, and saves the results to the filename given as the second command line argument.



## Two ways of commanding Twitter RBCPApp
### Way 1. Js Message

Commands can be sent to the program via `js message "twitter-rbcpapp>..."`. This interacts with the program's global state, allowing you to switch between different feeds. This feature was added to make the program easily usable without scripting.


### Way 2. twitterFeedClass commands

More control and customization is possible by using the provided *twitterFeedClass* object type. Basic usage is as follows:

```
        script include $Content/User/path/to/twitter-rbcpapp/twitter-rbcpapp-on.ds
+0.05   twitter_nasa is twitterFeedClass
        twitter_nasa twitterHandle "NASA"
        js message "twitter-rbcpapp>add twitter_nasa"
```

Then call commands on the object you created such as:

```
        twitter_nasa fetch
```

```
        twitter_nasa next
```

#### twitterFeedClass is type sceneObject

This feature allows you to put your twitterFeedClass object right into the scene as a child of some object. Templates could then make use of this for interesting effects. I have not really used this feature in practice yet, but plan to develop support for media items attached to scene objects in future versions.

#### twitterFeedClass customizeScript

The easiest way to add templates to the system is with a *customizeScript*. This is a js script that will be evalled into twitter-rbcpapp, allowing you to customize pretty much anything without modifying the original program, on a per-show or per-feed basis.

TO-DO: include an example customize-script in the source repository.


##### customizeScript API

**filter_register(fn);**
: A function of two arguments, twitter_handle and tweet. It should return true if this tweet is okay to display, false to skip it.

**templates_register(templates);**
: Templates is an array of template definitions. Each template definition is an object:

```
{ name: "templatename",
  predicate: function (tweet) { ... },
  on: function (twitterob, tweet, u) { ... },
  off: function (twitterob, tweet, u) { ... }
}
```

  * Name is a name for the template.
  * Predicate is a function that returns true if this template should be used for this tweet, false if not.
  * On is a function to display the tweet. Its first argument is the name of the twitterFeedClass object associated with this tweet. Its second argument is the tweet object. Its third argument is a uniquifier to append to the names of any objects it creates, to give reasonable assurance that they will be unique.
  * Off is a function to turn off the display of the tweet. It accepts the same arguments as the on function.


**text_template_register(fn);**
: Function is of two arguments, tweet and tweet_blacklisted_p. This function will be used to handle display of the tweet text. This is for putting the text into the control panel, if you want to do so in a different way than the default. For putting text on the dome, use a regular template.

**tweet_hide_hook.push(fn);**
: Function is of one argument, dsob, which is the twitterFeedClass object being hidden. The purpose of this is if you use text_template_register, you will probably also want to put a cleanup function here.



## Using Twitter RBCPApp via the provided control panel page

The provided control panel page is an example of how to use this app in a very basic way. The basic process is as follows:

1. The ON script sets up the class and objects used by the program and starts twitter-rbcpapp.js.
1. The program accepts commands in two modes, via *js message* or via a *twitterFeedClass* object. The buttons on the supplied control panel page all use *js message*.
1. Some interesting space-themed feeds are provided, but you can add other feeds by copying and modifying one of the existing feed scripts.
1. *Cache Info* reports how many tweets are in your cache.
1. *Fetch* downloads new tweets via the helper program *twitter-fetch.ps1* as explained above. By default a 10 minute minimum fetch interval is enforced.
1. The various *blacklist* buttons manage blacklisted tweets. As the name suggests, blacklisted tweets will not be displayed unless you turn on *Show Blacklisted*


## Version History

- **2.1.1** - *2020-09-22* - Fix crash when there are no tweets in a feed.
- **2.1.0** - *2020-09-10* - Fetchmedia command added.
- **2.0.0** - *2020-08-29* - Support for cloud upload and more support for global operation mode.
- **1.0.0** - *2020-08-27* - Initial release


## Help

Please feel free to contact me, John Foerch jjfoerch@grpm.org, with any questions, bug reports, or enhancement ideas.

